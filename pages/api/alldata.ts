import Product from "../../models/Product"
import initDB from "../../models/config"
initDB();
import {NextApiRequest,NextApiResponse} from 'next'
export default function(req:NextApiRequest, res:NextApiResponse) {
Product.find().then(Products=>{
  res.status(200).json(Products)
})
}