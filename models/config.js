// const mongoose =  require("mongoose")
// mongoose.connect('mongodb+srv://nikhilesh:nikhilesh@cluster0.7eeod.gcp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
// {
//     useNewUrlParser:true,
//     useUnifiedTopology:true,
//     useCreateIndex:true
// }).then(() => {
//     console.log("connection sucess")
// }).catch((e)=>{
// console.log("no connection")
// })
import mongoose from 'mongoose'

function initDB(){
    if(mongoose.connections[0].readyState){
        console.log("alredy connected")
        return
    }
    mongoose.connect(process.env.MONGO_URI,{
        useNewUrlParser:true,
        useUnifiedTopology:true
    })
    mongoose.connection.on('connected',()=>{
        console.log("connected to mongo")
    })
    mongoose.connection.on('error',(err)=>{
        console.log("error connecting",err)
    })
}


export default initDB