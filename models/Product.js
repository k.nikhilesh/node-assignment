
const mongoose = require('mongoose')

const productsSchema = new mongoose.Schema(
    {
        name : {
            type: String,
            required:true
        },
        Email : {
            type:String,
            required:true,
             unique:true
        },
        number : {
            type:String,
            required:true,
             unique:true
        },
        password : {
            type:String,
            required:true
        },
        image : {
            type:String,
            required:true
        }
    }
)

// const User = new mongoose.model("users",employeeSchema)
// module.exports = User
export default mongoose.models.product || mongoose.model('product',productsSchema)